function initializeMap() {
  var mapCanvas = document.getElementById('map-canvas');
  var myLatlng = new google.maps.LatLng(43.644107, -79.396861);
  var mapOptions = {
    center: myLatlng,
    zoom: 17,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    scrollwheel: false
  };
  var map = new google.maps.Map(mapCanvas, mapOptions);
  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      text: 'Le Select Bistro'
  });
}
google.maps.event.addDomListener(window, 'load', initializeMap);


$(function() {

  // Guest toggle
  $('input[name=has_guest]').change(function() {
    if ($(this).prop('checked')) {
      $('.rsvp__field--guest').removeClass('rsvp__field--hidden');
    }
    else {
      $('.rsvp__field--guest').addClass('rsvp__field--hidden');
    }
  });

  // Smooth scroll
  $('.navbar-links').click(function() {
    var id = $(this).data('target-id');
    if (id) {
      $('html, body').animate({
        scrollTop: $('#' + id).offset().top - 50
      }, 1000);
    }
  });

  $('form').submit(function(e) {
    e.preventDefault();

    $('input[type=submit]').attr('disabled', 'disabled');
    $('input[type=submit]').val('Sending..');

    var rsvp = {
      is_attending: $('input[name=is_attending]').val(),
      name: $('input[name=name]').val(),
      has_guest: $('input[name=has_guest]').val(),
      guest: $('input[name=guest]').val(),
      comment: $('input[name=comment]').val()
    };

    $.ajax({
      method: 'POST',
      url: 'http://stephanie.emila.ca/rsvp',
      data: $('form').serializeArray(),
      success: function(res) {
        console.log(res);
        $('form').addClass('hidden');
        $('.thank-you').removeClass('hidden');
      },
      error: function(res) {
        console.log(res);
        $('form').addClass('hidden');
        $('.error').removeClass('hidden');
      }
    });

    return false;
  });
});
